#!/bin/bash

#Installation
REMOTE_USER="ubuntu"
REMOTE_HOST="35.168.17.99"
SSH_KEY="/home/user/.ssh/adon.pem"

ssh -o StrictHostKeyChecking=no -i "$SSH_KEY" "$REMOTE_USER@$REMOTE_HOST" << 'EOF'
  sudo apt-add-repository -y ppa:ansible/ansible
  sudo apt-get update
  sudo apt-get install -y ansible
EOF